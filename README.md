# RAJ-Personal_AI_Assistant
It is a Python Based AI Assistant. 
### Created and Tested on Python 3.8 by @thesaahilraj


## What RAJ can do?

##### Tell Time
##### Tell Today's Date
##### Search on Wikipedia
##### Search on Google
##### Open Websites
##### Search Video on Youtube
##### Tell Jokes
##### Take Notes
##### Show Saved Notes
##### Take Screenshot
##### Remember Something
##### Tell Latest News Headlines
##### Show Location on Google Maps
##### Log off User
##### Restart Device
##### Shutdown Device

#### More to be Added in Upcoming Commits


## Requirements:

Make sure to install these packages before proceeding.

You can run :  `pip install -r requirements.txt` to install them all.

## Download .exe file using the Link Below
https://gofile.io/d/eU4tDZ
